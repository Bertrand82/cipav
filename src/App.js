import './App.css';
import Cipav from './Cipav.js'
import DocRecuesCipav from './DocRecuesCipav';
function App() {
  return (
    <div className="App">

      <h2>Cipav simu  </h2>
      <Cipav></Cipav>
      <div className="div_text"> <h2>Liens utiles </h2>
        <p> <a href="https://www.lacipav.fr/profession-liberale/professionnel-liberal-classique/calcul-cotisations">Calcul cotisation</a> Explication des règles de calcul des cotisations</p>
        <p> <a href="https://espace-personnel.lacipav.fr/actualite/61016ac4c862b0080fa34b62">Revision</a> Le revenu estimé est le revenu que vous « estimez » réaliser en 2021.
          Si vous en faites la demande, votre cotisation de retraite de base sera calculée directement en fonction de ce revenu « estimé ». </p>
        <p> <a href="https://espace-personnel.lacipav.fr/cotisations">Soldes et versements</a> Soldes déjà versés</p>
        <p> <a href="https://espace-personnel.lacipav.fr/detail-cotisations"> detail cotisation</a> Le detail des cotisations des 5 dernières années</p>
      </div>

     

      <div className="div_text">
        <h2> Mes problèmes personnels </h2>
        <ul>
        <li>Ma cotisation prévisionelle 2021 est de 7107 Euros (Votre courrier du 15 Decembre 2020 sur une base de revenu de 46040), ou 5515 (Votre courrier du 18 /09 2021 sur une base de revenue estimé de 25000 ) .</li>
        <li>La somme de  mes versements 2021 (19081.27 Euros) (Voir page "mes versements" du site lacipav.fr) sont largement superieurs à ma cotisation provisionnelle (13199 Euros ) et ma regulation 2020 (4405 Euros) (Voir page ma cotisation provisionnelle)</li>
          <li>Dans la page "Révision de vos cotisations" , vous demandez si "Vous souhaitez cotiser au plus près de vos revenus actuels ". J'ai toujours estimé mon revenu 2021 à 1 euro, puis à 0 Euros. Vous n'en avez jamais tenu compte! Vous avez (peut être ) prise comme base mes revenus de l'année precédente!</li>
          <li>Dans la page "detail-cotisation" Le reste à payer (3631 Euros) n'est pas coherent avec la somme de mes versements!</li>
          <li>Dans la page "detail-cotisation" , vous notez bien le revenu estimé (1 Euro dans mon cas) mais manifestement vous prenez un autre chifre pour faire vos calculs</li>
          <li>Dans la page "detail-cotisation" , pour vérifier vos calculs, il faudrait connaitre le "revenu estimé" que vous retenez (C'est un peu redondant avec la la critique ci-dessus )</li>
          <li>Au total, en 2021, La Cipav m'a' prélevé 19000 euros (D'après votre page "mes versements" , et  me programme (votre courrier d'octobre) 20118 Euros soit au total près de 40000 Euros de cotisation! Evidemment que mon resultat prévisionnel est négatif! (CA de moin de 40000Euro en 2020)</li>
          <li>Le prélèvement du mois de Novembre a été de 3631.46 . Et non de 9850 comme annoncé (courrier Octobre 21). C'est mieux pour moi, certe, mais très mysterieux .</li>
        </ul>
      </div>

     
      <DocRecuesCipav/>

      <div className="div_text"> <h2>Bugs, critiques et idées d'amelioration du site en ligne de  <a href="https://www.lacipav.fr/">lacipav.fr</a></h2>
        <a href="https://www.lacipav.fr/profession-liberale/professionnel-liberal-classique/calcul-cotisations">Calcul de mes cotisation</a>
          <ul>
            <li>Il y a un effet de seuil brutal sur le calcul de la retraite de base: entre un revenu de  41136 et 41137, il y a une difference de cotisation de 41136*1,87% soit près de 900 Euros de cotisation! Waouh!</li>
          </ul>
        <a href="https://espace-personnel.lacipav.fr/cotisations">Soldes et Versement</a>
        <ul>
          <li>Dans l'onglet "Mes versements", la liste des versements devrait être ordonné par date . </li>
          <li>Dans l'onglet "Mes versementse", le total des versements devrait aparaître. Cela serait plus sympa.</li>

        </ul>
        <a href="https://espace-personnel.lacipav.fr/detail-cotisations">Detail Cotisation</a>
        <ul>
        <li> Dans la page "detail-cotisation ", ce serait sympa de faire l'addition (regulation + mes cotisations previsionnelles) </li>
        <li> Dans la page "detail-cotisation ", le "revenu estimé" ne sert pa de base au calcul des cotisations présentées</li>
        <li> Les courriers envoyés par la Cipav n'ont pas de références . C'est difficile de s'y retouver</li>
        </ul>
      </div>

    </div>
  );
}
export default App;
