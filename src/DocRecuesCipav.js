const React = require('react');

const docuCipav =[
    {
        "nameFile": "doc_20190110_0_jcr_content.pdf",
        "name": "jcr content",
        "url": "docs/doc_20190110_0_jcr_content.pdf",
        "date": "20190110",
        "date2": "10 janvier 2019",
        "objet": "ATTESTATION",
        "auteur": "",
        "textCorps": "ré-affilié à la CIPAV avec effet du 1er avril 2015 sous le numéro 2008-4411238-3-90 pour une activité de developpement de logiciels, a réglé toutes les cotisations exigibles au 31 décembre 201"
    },
    {
        "nameFile": "doc_20201215_0_Votre_echeancier_de_prelevement_2021_20201216142240.pdf",
        "name": "Votre echeancier de prelevement 2021 20201216142240",
        "url": "docs/doc_20201215_0_Votre_echeancier_de_prelevement_2021_20201216142240.pdf",
        "date": "20201215",
        "date2": "15 décembre 2020",
        "objet": "votre échéancier de prélèvement 2021",
        "auteur": "",
        "textCorps": "Total de vos cotisations 2021 : 7 107,00 € sur base Revenu: 46 040,00  (revenus 2019 servantde base) ",
        "comment": "Interessant car fournit un exemple de la manière dont sont calculées les cotisations. Revenu de base (2019): 46040,00;Retraite de base tranche1 : 3825,0 (ok) Pour la tranche 2 ,861.0 c'est le revenue entier * 1.87 et non la \"tranche\"",
    },
    {
        "nameFile": "doc_20210114_0_Rejet_de_prélèvement_14081130.pdf",
        "name": "Rejet de prélèvement 14081130",
        "url": "docs/doc_20210114_0_Rejet_de_prélèvement_14081130.pdf",
        "date": "20210114",
        "date2": "14 janvier 2021",
        "objet": "Premier rejet de prélèvement",
        "auteur": "",
        "textCorps": "Nous vous informons que le prélèvement de janvier 2021 dont le montant est de 592,25 € n'a pas puêtre honoré pour le motif suivant : COMPTE SOLDÉ CLOTURÉ VIRÉ / COMPTE CLOTURÉ.Nous vous demandons de bien vouloir régulariser votre situation dans les plus brefs délais afin quenous puissions prélever au mois de février 2021 les mensualités de janvier et de février, soit unesomme globale de 1 184,50 €. A cet égard, nous vous invitons à vous reporter au dernier échéancierqui vous a été communiqué.",
        "comment": "Mon compte existe depuis plus de 20 ans"
    },
    {
        "nameFile": "doc_20210419_0_Attestation_fiscale_20210423121243.pdf",
        "name": "Attestation fiscale 20210423121243",
        "url": "docs/doc_20210419_0_Attestation_fiscale_20210423121243.pdf",
        "date": "20210419",
        "date2": "19 avril 2021",
        "objet": "",
        "auteur": "",
        "textCorps": "Le montant des cotisations versées au cours de l’année 2020 s’élève à 1 514,16 €",
        "comment": "A vérifier"
    },
    {
        "nameFile": "doc_20210611_0_Rejet_de_prélèvement_11081236.pdf",
        "name": "Rejet de prélèvement 11081236",
        "url": "docs/doc_20210611_0_Rejet_de_prélèvement_11081236.pdf",
        "date": "20210611",
        "date2": "11 juin 2021",
        "objet": "Premier rejet de prélèvement",
        "auteur": "",
        "textCorps": "Nous vous informons que le prélèvement de juin 2021 dont le montant est de 657,73 € n'a pas puêtre honoré pour le motif suivant : COORD. BANC. INEXPLOITABLE.Nous vous demandons de bien vouloir régulariser votre situation dans les plus brefs délais afin quenous puissions prélever au mois de juillet 2021 les mensualités de juin et de juillet, soit une sommeglobale de 1 315,46 €. A cet égard, nous vous invitons à vous reporter au dernier échéancier quivous a été communiqué.",
        "comment": "Pour mémoire, mes coordonnées bancaires sont inchangés depuis plus de 20 ans!"
    },
    {
        "nameFile": "doc_20210614_0_jcr_content.pdf",
        "name": "jcr content",
        "url": "docs/doc_20210614_0_jcr_content.pdf",
        "date": "20210614",
        "date2": "14 Juion 2021",
        "objet": "Modification du revenu estimé",
        "auteur": "",
        "textCorps": "Je demande une révision car j'estime mon revenu à 25000"
    },
    {
        "nameFile": "doc_20210918_0_Votre_appel_de_cotisations_2021_20210920150322.pdf",
        "name": "Votre appel de cotisations 2021 20210920150322",
        "url": "docs/doc_20210918_0_Votre_appel_de_cotisations_2021_20210920150322.pdf",
        "date": "20210918",
        "date2": "18 Septembre 2021",
        "objet": "",
        "auteur": "",
        "textCorps": "Vous venez de déclarer votre revenu d’activité indépendante de l’année 2020 et avez communiqué une estimation de vos revenus 2021 .Sur cette base, nous vous adressons votre appel unique qui comprend : la régularisation de vos cotisations 2020 ; le calcul de vos cotisations 2021 ; l’estimation de vos cotisations 2022.Le montant annuel des cotisations au titre de l’année 2021, assorti de l’éventuelle régularisation des cotisations de l’année 2020, dontvous trouverez le détail au verso, s’élève à  12  916,00 €. Compte tenu de vos précédents versements, vous êtes redevable de la sommede 12 837,19 €.Pour mémoire, le prélèvement mensuel de vos cotisations est effectué sur le compte bancaire indiqué ci-dessous :M. GUIRAL BERTRAND IBAN : FR76102680258232116600300072 RUE CARIBEN82270 MONTPEZAT DE QUERCYEn cas de changement de coordonnées bancaires, nous vous remercions d’en informer la Cipav par écrit, en joignant un nouveau RIBau format IBAN.Ci-dessous le détail de vos nouvelles mensualités :Périodes Mensualités10 février 2021578,92 €10 mars 2021657,73 €10 avril 2021657,73 €10 mai 2021160,81 €10 juin 2021230,13 €10 juillet 2021468,00 €10 août 20211 425,68 €10 octobre 2021418,73 €10 novembre 2021419,23 €10 décembre 2021419,23 €9 rue de Vienne – 75403 Paris cedex 08 – T. 01 44 95 68 20Nous sommes ouverts du lundi au vendredi. Pour consulter nos horaires, rendez-vous sur www.lacipav.fr",
        "comment" : "Liste des echéances revenus estimés :25 000,00 € , montant à régler : 12 837,19 €, cotisations 2021 :5515 (Revenu estimé 2021 25000 Euros) ; Regularisation 2020 7401 Euros; total 12837 Euros. c'est correct"
    },
    {
        "nameFile": "doc_20211015_0__Revision.pdf",
        "name": "Revision",
        "url": "docs/doc_20211015_0__Revision.pdf",
        "date": "20211015",
        "date2": "15 octobre 2021",
        "objet": "Révision de cotisations",
        "auteur": "",
        "textCorps": "À compter de 2021, votre cotisation de retraite complémentaire est  régularisée dans les mêmes conditionsque celle du régime de base.",
        "comment": ""
    },
    {
        "nameFile": "doc_20211029_0_Reponse_a_votre_demande.pdf",
        "name": "Reponse a votre demande",
        "url": "docs/doc_20211029_0_Reponse_a_votre_demande.pdf",
        "date": "20211029",
        "date2": "29 octobre 2021",
        "objet": "Estimation de revenus",
        "auteur": "Sonia LARHZIZAL",
        "textCorps": "Nous faisons suite à votre estimation de revenus 2021 de 1 euros effectuer sur votre espace adhérent en ligne lacipav.fren date de 14/06/2021.Le revenu estimé est le revenu que vous « estimez » réaliser en 2021. Si vous en faites la demande, votre cotisation deretraite de base sera calculée directement en fonction de ce revenu « estimé ».Dans un second temps, votre cotisation sera révisée lorsque nous aurons connaissance de vos revenus définitifs pourl'exercice de l'année 2021, soit en 2022.Attention, si votre revenu définitif excède de plus d'un tiers le revenu estimé, vous ferez l'objet, lors de la régularisation dela cotisation, d'une majoration de 5 % à 10 % pour insuffisance de versement de vos acomptes provisionnels.Nous vous prions d’agréer, Monsieur, l’expression de notre considération distinguée.            "
    },
    {
        "nameFile": "doc_20211029_1_Explication_des_cotisations.pdf",
        "name": "Explication des cotisations",
        "url": "docs/doc_20211029_1_Explication_des_cotisations.pdf",
        "date": "20211029",
        "date2": "29 octobre 2021",
        "objet": "explications du calcul de vos cotisations 2021",
        "auteur": "Sonia LARHZIZAL",
        "textCorps": "Nous faisons suite à votre demande d'explications sur le calcul de vos cotisations.Au regard de votre activité vous devez cotiser à 3 régimes qui sont tous obligatoires selon les modalités précisées ci-dessous.eNous attirons votre attention sur les revenus servant de base au calcul de vos cotisations à compter de la 2 annéed'affiliation. Votre cotisation de retraite de base se calcule comme suit :Revenus 2020 Cotisations 2021Inférieurs à 4 731 € Forfait minimum de 477 € 8,23% de vos revenus compris entre 0 € et 41 136 € (plafond tranche 1 en 2021)Supérieurs à 4 731 € 1,87% de vos revenus compris entre 0 € et 205 680 € (plafond tranche 2 en 2021) Votre cotisation de retraite complémentaire est appelée dans l’une des huit classes en fonction de vos revenus :Tranches de revenus 2020 Classes Cotisations 2021 Points attribuésJusqu’à 26 580 € Classe A 1 457 € 36De 26 581 € à 49 280 € Classe B 2 913 € 72De 49 281 € à 57 850 € Classe C 4 370 € 108De 57 851 € à 66 400 € Classe D 7 283 € 180De 66 401 € à 83 060 € Classe E 10 196 € 252De 83 061 € à 103 180 € Classe F 16 023 € 396De 103 181 € à 123 300 € Classe G 17 479 € 432Supérieurs à 123 300 € Classe H 18 936 € 468 Votre cotisation de prévoyance invalidité-décès :Classes Classe A Classe B Classe CCotisation définitive 2021 76 € 228 € 380 €",
        "comment": "",
    },
    {
        "nameFile": "doc_20211109_0_Votre demande_de_delai.pdf",
        "name": "Votre demande de delai",
        "url": "docs/doc_20211109_0_Votre demande_de_delai.pdf",
        "date": "20211109",
        "date2": "9 novembre 2021",
        "objet": "échéancier de paiement",
        "auteur": "",
        "textCorps": "Nous vous rappelons que vous avez la possibilité de souscrire au paiement en 3 fois sur votre espace adhérent, pourchaque année de cotisations dont vous êtes redevable.Si toutefois vous souhaitez solliciter une période d'étalement plus importante, ..... ",
        "comment": "",
    }
]
class DocRecuesCipav extends React.Component {
    render() {
        return (

        <div  className="div_text">
            Doc Recues de la  Cipav
            <table border="1">
            {docuCipav.map((d)=>(<tr><td><a href={d.url}>{d.name}</a></td><td>{d.date2}</td> <td>{d.objet}</td> <td>{d.textCorps}</td><td>{d.comment}</td></tr>))}
            </table>
            
            </div>
            
            );
    }
}

export default DocRecuesCipav;