const React = require('react');

const c_cotisation_base = {
    seuil_1_cotisation_base: 4731,
    seuil_2_cotisation_base: 41136,
    seuil_3_cotisation_base: 205680 ,
    forfait_minimum_cotisation_base: 477,
    plafond_tranche_1_cotisation_base: 41136,
    taux_1: 0.0823,
    taux_2: 0.0187,
}
const c_class = {
    seuil_1: 26580,
    seuil_2:49280,
    seuil_3:57850,
    seuil_4: 66400,
    seuil_5: 83060,
    seuil_6: 103180,
    seuil_7: 123300,
    class_A: 1457 ,
    class_B: 2913 ,
    class_C: 4370 ,
    class_D: 7283 ,
    class_E: 10196 ,
    class_F: 16023 ,
    class_G: 17479 ,
    class_H: 18936 ,
}
class Cipav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            revenue: 69576,           
        }
        this.updateRevenue = this.updateRevenue.bind(this)
        this.updateRevenue2 = this.updateRevenue2.bind(this)
        this.updateRevenue2(70000);
    }

    updateRevenue(e) {
        console.log("update revenue", e.target.value);
        let r = e.target.value;
        this.updateRevenue2(r);
    }
    updateRevenue2(revenue0){
        console.log("update revenue2 "+revenue0);
        let cotisation0 = this.calculRetraite(revenue0);
        this.setState(
            cotisation0
        )
    }

    calculRetraite(revenue0) {
        console.log("update revenue2 "+revenue0);
        let retraiteBase_1_ = 0;
        let retraiteBase_2_ = 0;
        let retraiteBase_3_ = 0;
        let assiette1 =0;
        let assiette2=0;
        if (revenue0 < c_cotisation_base.seuil_1_cotisation_base) {
            retraiteBase_1_ = c_cotisation_base.forfait_minimum_cotisation_base;
        } else {
            assiette1 = Math.min(c_cotisation_base.seuil_2_cotisation_base,revenue0);           
            retraiteBase_2_ = assiette1*c_cotisation_base.taux_1;   
        }
        if (revenue0 > c_cotisation_base.seuil_2_cotisation_base){
            assiette2 = Math.min(revenue0,c_cotisation_base.seuil_3_cotisation_base);
            retraiteBase_3_ = assiette2*c_cotisation_base.taux_2;
        }
        let retraite_complementaire_class_A =0;
        let retraite_complementaire_class_B =0;
        let retraite_complementaire_class_C =0;
        let retraite_complementaire_class_D =0;
        let retraite_complementaire_class_E =0;
        let retraite_complementaire_class_F =0;
        let retraite_complementaire_class_G =0;
        let retraite_complementaire_class_H =0;
        let revenue = revenue0;
        let cotisationInvaliditeDeces =76 ;// Minimum classe A
        if (revenue< c_class.seuil_1){
            retraite_complementaire_class_A=c_class.class_A;
        }else if (revenue <c_class.seuil_2){
            retraite_complementaire_class_B=c_class.class_B;
        }else if (revenue <c_class.seuil_3){
            retraite_complementaire_class_C=c_class.class_C;
        }else if (revenue <c_class.seuil_4){
            retraite_complementaire_class_D=c_class.class_D;
         }else if (revenue <c_class.seuil_5){
            retraite_complementaire_class_E=c_class.class_E;
         }else if (revenue <c_class.seuil_6){
            retraite_complementaire_class_F=c_class.class_F;
        }else if (revenue <c_class.seuil_7){
            retraite_complementaire_class_G=c_class.class_G;
        }else {
            retraite_complementaire_class_H=c_class.class_H;
        }
        let retraiteBaseTotal = (retraiteBase_1_+retraiteBase_2_+retraiteBase_3_);
        let retraiteComplementaireTotal = retraite_complementaire_class_A+retraite_complementaire_class_B+retraite_complementaire_class_C+retraite_complementaire_class_D+retraite_complementaire_class_E+retraite_complementaire_class_F+retraite_complementaire_class_G+retraite_complementaire_class_H;
        let totalCipav= retraiteComplementaireTotal+cotisationInvaliditeDeces+retraiteBaseTotal
        let cotisations = {
            assiette1: assiette1.toFixed(2),
            assiette2: assiette2.toFixed(2),
            revenue: revenue0,
            retraiteBase_1: retraiteBase_1_.toFixed(2),
            retraiteBase_2: retraiteBase_2_.toFixed(2),
            retraiteBase_3: retraiteBase_3_.toFixed(2),
            retraiteBaseTotal: retraiteBaseTotal.toFixed(2),
            retraite_complementaire_class_A: retraite_complementaire_class_A.toFixed(2),
            retraite_complementaire_class_B: retraite_complementaire_class_B.toFixed(2),
            retraite_complementaire_class_C: retraite_complementaire_class_C.toFixed(2),
            retraite_complementaire_class_D: retraite_complementaire_class_D.toFixed(2),
            retraite_complementaire_class_E: retraite_complementaire_class_E.toFixed(2),
            retraite_complementaire_class_F: retraite_complementaire_class_F.toFixed(2),
            retraite_complementaire_class_G: retraite_complementaire_class_G.toFixed(2),
            retraite_complementaire_class_H: retraite_complementaire_class_H.toFixed(2),
            retraiteComplementaireTotal: retraiteComplementaireTotal.toFixed(2),
            cotisationInvaliditeDeces:cotisationInvaliditeDeces,
            totalCipav:totalCipav.toFixed(2),
                  
        };
        return cotisations;

    } 

    componentDidMount() {
        console.log("componentDidMount -------------");
        this.updateRevenue2(this.state.revenue);
    }

    render() {
        return (
            <div>
                <h2> Calcul des cotisation 2021 </h2>
                <div>Revenu estimé : <input value={this.state.revenue} onChange={this.updateRevenue} /> (Revenu de l'année précédente par défaut )</div>
                <table border="1">
                    <tbody>
                    
                    <tr><td colspan="5">Retraite de Base </td></tr>
                    <tr><td></td><td>Si revenues inferieurs à {c_cotisation_base.seuil_1_cotisation_base}  </td><td className="td2">{this.state.retraiteBase_1}</td></tr>
                    <tr><td></td><td>Tranche 1 inferieure à 41136 (taux 8.23%): ({this.state.assiette1})  </td><td className="td2">{this.state.retraiteBase_2}</td></tr>
                    <tr><td></td><td>Tranche 2 Supérieure à 41136 (taux 1.87%): ({this.state.assiette2})  </td><td className="td2">{this.state.retraiteBase_3}</td></tr>
                    <tr><td></td><td>Total retraite de base </td> <td className="td2 td_total">{this.state.retraiteBaseTotal}</td></tr>
                    <tr><td colSpan="5">Retraite Complémentaire ( Votre cotisation de retraite complémentaire est appelée dans l’une des huit classes en fonction de vos revenus ) </td></tr>
                    <tr><td></td><td>classe A jusqu'a {c_class.seuil_1}</td><td className="td2">{this.state.retraite_complementaire_class_A}</td></tr>
                    <tr><td></td><td>classe B entre {c_class.seuil_1} et  {c_class.seuil_2} </td><td className="td2">{this.state.retraite_complementaire_class_B}</td></tr>
                    <tr><td></td><td>classe C entre {c_class.seuil_2} et  {c_class.seuil_3} </td><td className="td2">{this.state.retraite_complementaire_class_C}</td></tr>
                    <tr><td></td><td>classe D entre {c_class.seuil_3} et  {c_class.seuil_4} </td><td className="td2">{this.state.retraite_complementaire_class_D}</td></tr>
                    <tr><td></td><td>classe E entre {c_class.seuil_4} et  {c_class.seuil_5} </td><td className="td2">{this.state.retraite_complementaire_class_E}</td></tr>
                    <tr><td></td><td>classe F entre {c_class.seuil_6} et  {c_class.seuil_7} </td><td className="td2">{this.state.retraite_complementaire_class_F}</td></tr>
                    <tr><td></td><td>classe G supérieur à {c_class.seuil_7}                 </td><td className="td2">{this.state.retraite_complementaire_class_G}</td></tr>
                    <tr><td></td><td>Total retraite Complementaire </td> <td className="td2 td_total">{this.state.retraiteComplementaireTotal}</td></tr>
                    <tr><td></td><td>Total Invalidite deces (minimum classe A)</td> <td className="td2 td_total">{this.state.cotisationInvaliditeDeces}</td></tr>
                  
                    <tr><td></td><td>Total cotisations Cipav de l'année: </td><td></td><td className="td2"> {this.state.totalCipav}  </td></tr>
                    
                     </tbody>
                </table>
            </div>
        )
    }
}

export default Cipav;